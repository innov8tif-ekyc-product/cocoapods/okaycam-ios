// swift-interface-format-version: 1.0
// swift-compiler-version: Apple Swift version 6.0.3 effective-5.10 (swiftlang-6.0.3.1.10 clang-1600.0.30.1)
// swift-module-flags: -target x86_64-apple-ios15.6-simulator -enable-objc-interop -enable-library-evolution -swift-version 5 -enforce-exclusivity=checked -O -enable-bare-slash-regex -module-name OkayCam
// swift-module-flags-ignorable: -no-verify-emitted-module-interface
import AVFoundation
import CoreImage
import CoreLocation
import CoreMotion
import CryptoSwift
import DeveloperToolsSupport
import Foundation
import ImageIO
import MobileCoreServices
@_exported import OkayCam
import Photos
import PhotosUI
import Swift
import SwiftUI
import UIKit
import WebKit
import _Concurrency
import _StringProcessing
import _SwiftConcurrencyShims
extension UIKit.UIApplication {
  @_Concurrency.MainActor @preconcurrency public var isSplitOrSlideOver: Swift.Bool {
    get
  }
}
public enum CameraState {
  case ready, accessDenied, noDeviceFound, notDetermined
  public static func == (a: OkayCam.CameraState, b: OkayCam.CameraState) -> Swift.Bool
  public func hash(into hasher: inout Swift.Hasher)
  public var hashValue: Swift.Int {
    get
  }
}
public enum CameraDevice {
  case front, back
  public static func == (a: OkayCam.CameraDevice, b: OkayCam.CameraDevice) -> Swift.Bool
  public func hash(into hasher: inout Swift.Hasher)
  public var hashValue: Swift.Int {
    get
  }
}
public enum CameraFlashMode : Swift.Int {
  case off, on, auto
  public init?(rawValue: Swift.Int)
  public typealias RawValue = Swift.Int
  public var rawValue: Swift.Int {
    get
  }
}
public enum CameraOutputMode {
  case stillImage, videoWithMic, videoOnly
  public static func == (a: OkayCam.CameraOutputMode, b: OkayCam.CameraOutputMode) -> Swift.Bool
  public func hash(into hasher: inout Swift.Hasher)
  public var hashValue: Swift.Int {
    get
  }
}
public enum CaptureResult {
  case success(content: OkayCam.CaptureContent)
  case failure(any Swift.Error)
}
public enum CaptureContent {
  case imageData(Foundation.Data)
  case image(UIKit.UIImage)
  case asset(Photos.PHAsset)
}
extension OkayCam.CaptureContent {
  public var asImage: UIKit.UIImage? {
    get
  }
  public var asData: Foundation.Data? {
    get
  }
}
public enum CaptureError : Swift.Error {
  case noImageData
  case invalidImageData
  case noVideoConnection
  case noSampleBuffer
  case assetNotSaved
  public static func == (a: OkayCam.CaptureError, b: OkayCam.CaptureError) -> Swift.Bool
  public func hash(into hasher: inout Swift.Hasher)
  public var hashValue: Swift.Int {
    get
  }
}
@_hasMissingDesignatedInitializers public class OkayCamSDK {
  public enum IdType {
    case id, passport
    public static func == (a: OkayCam.OkayCamSDK.IdType, b: OkayCam.OkayCamSDK.IdType) -> Swift.Bool
    public func hash(into hasher: inout Swift.Hasher)
    public var hashValue: Swift.Int {
      get
    }
  }
  @objc deinit
}
@_hasMissingDesignatedInitializers public class OkayCamDoc {
  public typealias FilesCompletion = ([Foundation.URL]?, OkayCam.OkayCamError?) -> Swift.Void
  public static func start(okayCamConfig config: OkayCam.OkayCamConfig, license: Swift.String, _ completion: @escaping OkayCam.OkayCamDoc.FilesCompletion)
  @objc deinit
}
public func convertImageToBase64(fileUrl: Foundation.URL) throws -> Swift.String
@_hasMissingDesignatedInitializers public class OkayCamSelfie {
  public typealias SingleFileCompletion = (Foundation.URL?, OkayCam.OkayCamError?) -> Swift.Void
  public static func start(okaySelfieConfig config: OkayCam.OkaySelfieConfig, license: Swift.String, _ completion: @escaping OkayCam.OkayCamSelfie.SingleFileCompletion)
  @objc deinit
}
public class OkayCamConfig {
  public var topLabel: OkayCam.OkayCamLabelConfig
  public var bottomLabel: OkayCam.OkayCamLabelConfig
  public var isLandscapeOnlyDoc: Swift.Bool
  public var showOverlay: Swift.Bool
  public var frame: OkayCam.OkayCamFrameConfig
  public var timer: OkayCam.OkayCamTimerConfig
  public var captureBtnColor: UIKit.UIColor
  public var captureConfigPair: OkayCam.CaptureConfigPair
  public var confirmBtnConfig: OkayCam.OkayCamBtnConfig
  public var retakeBtnConfig: OkayCam.OkayCamBtnConfig
  public var torchBtnEnabled: Swift.Bool
  public var crop: Swift.Bool
  public var width: Swift.Int?
  public var imageQuality: CoreFoundation.CGFloat
  public var fullScreen: Swift.Bool
  public var preview: OkayCam.OkayCamPreviewConfig?
  public var instruction: OkayCam.OkayCamInstruction?
  public init(viewController: UIKit.UIViewController)
  @objc deinit
}
public struct CaptureConfigPair {
  public init(firstPhoto: OkayCam.OkayCamCaptureConfig = OkayCamCaptureConfig(), secondPhoto: OkayCam.OkayCamCaptureConfig? = nil)
  public var firstPhoto: OkayCam.OkayCamCaptureConfig
  public var secondPhoto: OkayCam.OkayCamCaptureConfig?
}
public struct OkayCamFrameConfig {
  public init(size: CoreFoundation.CGSize? = nil, color: UIKit.UIColor = UIColor.white, content: Foundation.URL? = nil)
  public var size: CoreFoundation.CGSize?
  public var color: UIKit.UIColor
  public var content: Foundation.URL?
}
public struct OkayCamTimerConfig {
  public init(backgroundColor: UIKit.UIColor = UIColor.black, textColor: UIKit.UIColor = UIColor.white)
  public var backgroundColor: UIKit.UIColor
  public var textColor: UIKit.UIColor
}
public struct OkayCamBtnConfig {
  public static let defaultColor: UIKit.UIColor
  public init(backgroundColor: UIKit.UIColor = defaultColor, contentColor: UIKit.UIColor = UIColor.white)
  public var backgroundColor: UIKit.UIColor
  public var contentColor: UIKit.UIColor
}
public struct OkayCamPreviewConfig {
  public init(title: OkayCam.OkayCamLabelConfig, refImage: Foundation.URL, instruction1: OkayCam.OkayCamLabelConfig, instruction2: OkayCam.OkayCamLabelConfig, instruction3: OkayCam.OkayCamLabelConfig, backgroundColor: UIKit.UIColor)
  public var title: OkayCam.OkayCamLabelConfig
  public var refImage: Foundation.URL
  public var instruction1: OkayCam.OkayCamLabelConfig
  public var instruction2: OkayCam.OkayCamLabelConfig
  public var instruction3: OkayCam.OkayCamLabelConfig
  public var backgroundColor: UIKit.UIColor
}
public struct OkayCamInstruction {
  public init(title: OkayCam.OkayCamLabelConfig, refImage1: OkayCam.OkaycamInstructionRefImg, refImage2: OkayCam.OkaycamInstructionRefImg? = nil)
  public var title: OkayCam.OkayCamLabelConfig
  public var refImage1: OkayCam.OkaycamInstructionRefImg
  public var refImage2: OkayCam.OkaycamInstructionRefImg?
}
public struct OkaycamInstructionRefImg {
  public init(title: OkayCam.OkayCamLabelConfig, img: Foundation.URL)
  public var title: OkayCam.OkayCamLabelConfig
  public var img: Foundation.URL
}
public class OkaySelfieConfig {
  public var isLandscapeOnlySelfie: Swift.Bool
  public var topLabel: OkayCam.OkaySelfieLabelConfig
  public var bottomFrameColor: UIKit.UIColor
  public var captureBtnColor: UIKit.UIColor
  public var confirmBtnConfig: OkayCam.OkayCamBtnConfig
  public var retakeBtnConfig: OkayCam.OkayCamBtnConfig
  public var switchBtnConfig: OkayCam.SwitchBtnConfig
  public var defaultCameraFacing: OkayCam.CameraDevice
  public var outputPath: Foundation.URL?
  public var width: Swift.Int?
  public var imageQuality: CoreFoundation.CGFloat
  public init(viewController: UIKit.UIViewController)
  @objc deinit
}
public struct OkaySelfieLabelConfig {
  public init(text: Swift.String, color: UIKit.UIColor, size: CoreFoundation.CGFloat)
  public var text: Swift.String
  public var color: UIKit.UIColor
  public var size: CoreFoundation.CGFloat
}
public struct OkayCamLabelConfig {
  public init(text: Swift.String, color: UIKit.UIColor, size: CoreFoundation.CGFloat)
  public var text: Swift.String
  public var color: UIKit.UIColor
  public var size: CoreFoundation.CGFloat
}
public struct OkayCamSVGFileConfig {
  public init(path: Swift.String)
  public var path: Swift.String
}
public struct OkayCamCaptureConfig {
  public init(timeOut: Swift.Int = 0, onFlash: Swift.Bool = false, onImageQuality: Swift.Bool = false, outputPath: Foundation.URL? = nil)
  public var timeOut: Swift.Int
  public var onFlash: Swift.Bool
  public var onImageQuality: Swift.Bool
  public var outputPath: Foundation.URL?
}
public struct SwitchBtnConfig {
  public init(color: UIKit.UIColor = UIColor.white, show: Swift.Bool = false)
  public var color: UIKit.UIColor
  public var show: Swift.Bool
}
public enum OkayCamError : Swift.Error {
  case invalidConfig(_: Swift.String)
  case imageCapture(_: Swift.String)
  case userCancelled
  case invalidLicense
  case noError
  case cameraPermission
  case camera(_: Swift.String)
  case miscellaneous(_: Swift.String)
}
extension OkayCam.CameraState : Swift.Equatable {}
extension OkayCam.CameraState : Swift.Hashable {}
extension OkayCam.CameraDevice : Swift.Equatable {}
extension OkayCam.CameraDevice : Swift.Hashable {}
extension OkayCam.CameraFlashMode : Swift.Equatable {}
extension OkayCam.CameraFlashMode : Swift.Hashable {}
extension OkayCam.CameraFlashMode : Swift.RawRepresentable {}
extension OkayCam.CameraOutputMode : Swift.Equatable {}
extension OkayCam.CameraOutputMode : Swift.Hashable {}
extension OkayCam.CaptureError : Swift.Equatable {}
extension OkayCam.CaptureError : Swift.Hashable {}
extension OkayCam.OkayCamSDK.IdType : Swift.Equatable {}
extension OkayCam.OkayCamSDK.IdType : Swift.Hashable {}
