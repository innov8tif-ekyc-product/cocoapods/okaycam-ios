//
//  OpenCVUtils.h
//  OpenCVTest
//
//  Created by Ban Er Win on 27/07/2022.
//

#ifndef OpenCVUtils_h
#define OpenCVUtils_h

#include <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

//OpenCV

NS_ASSUME_NONNULL_BEGIN

@interface OpenCVUtils: NSObject
+ (NSString *)openCVVersionString;
-(NSString *) isImageBlurry:(UIImage *) image;
-(NSString *) isImageGlare:(UIImage *)image;
- (NSString *) isImageBright:(UIImage *)image;

//TorchModule Image Segmentation
- (nullable instancetype)initWithFileAtPath:(NSString*)filePath
    NS_SWIFT_NAME(init(fileAtPath:))NS_DESIGNATED_INITIALIZER;
+ (instancetype)new NS_UNAVAILABLE;
//- (instancetype)init NS_UNAVAILABLE;
//- (unsigned char*)segmentImage:(void*)imageBuffer withWidth:(int)width withHeight:(int)height
- (NSMutableArray *)segmentImage:(void*)imageBuffer withWidth:(int)width withHeight:(int)height
   NS_SWIFT_NAME(segment(image:withWidth:withHeight:));

@end

NS_ASSUME_NONNULL_END

#endif /* OpenCVUtils_h */
